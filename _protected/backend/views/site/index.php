<?php

/* @var $this yii\web\View */
$this->title = 'Admin Page';

$this->registerJsFile('@themes/default/js/pages/dashboard2.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@vendor/almasaeed2010/adminlte/plugins/chartjs/Chart.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@vendor/almasaeed2010/adminlte/plugins/chartjs/Chart.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
?>
<div class="site-index">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Monthly  Report</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

                        <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <p class="text-center">
                                <strong>แสดงงานซ่อม</strong>
                            </p>
                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="height: 180px;"></canvas>
                            </div><!-- /.chart-responsive -->
                        </div><!-- /.col -->
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>สถานะงานทั้งหมด</strong>
                            </p>
                            <div class="progress-group">
                                <span class="progress-text">ทั้งหมด</span>
                                <span class="progress-number"><b>1000</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua" style="width: 100%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">ยกเลิก</span>
                                <span class="progress-number"><b>100</b>/1000</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-red" style="width: 10%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">เสร็จเรียบร้อย</span>
                                <span class="progress-number"><b>300</b>/1000</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-green" style="width: 30%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                            <div class="progress-group">
                                <span class="progress-text">กำลังดำเนินงาน</span>
                                <span class="progress-number"><b>600</b>/1000</span>
                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-yellow" style="width: 60%"></div>
                                </div>
                            </div><!-- /.progress-group -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- ./box-body -->

            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
    <!-- TABLE: LATEST ORDERS -->
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Latest Ticket</h3>
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
        </div><!-- /.box-header -->
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                    <tr>
                        <th>Ticket ID</th>
                        <th>หัวข้อ</th>
                        <th>รายละเอียด</th>
                        <th>สถานะงาน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><a href="pages/examples/invoice.html">FW1234</a></td>
                        <td>Internet</td>
                        <td>เครื่องใช้ที่1 ไม่ สามารถติดต่ออกันได้</td>
                        <td><span class="label label-success">เสร็จสิ้น</span></td>

                    </tr>
                    <tr>
                        <td><a href="pages/examples/invoice.html">FW1234</a></td>
                        <td>เครื่องใช้สำนักงาน</td>
                        <td>เครื่องใช้ที่1 ไม่ สามารถติดต่ออกันได้</td>
                        <td><span class="label label-warning">กำลังดำเนินงาน</span></td>
                    </tr>
                    <tr>
                        <td><a href="pages/examples/invoice.html">FW1234</a></td>
                        <td>งานก่อสร้าง</td>
                        <td>เครื่องใช้ที่1 ไม่ สามารถติดต่ออกันได้</td>
                        <td><span class="label label-danger">ยกเลิก</span></td>
                    </tr>
                    <tr>
                        <td><a href="pages/examples/invoice.html">FW1234</a></td>
                        <td>เครื่องใช้ไฟฟ้า</td>
                        <td>เครื่องใช้ที่1 ไม่ สามารถติดต่ออกันได้</td>
                        <td><span class="label label-info">รอรับงาน</span></td>
                    </tr>
                    <tr>
                        <td><a href="pages/examples/invoice.html">FW1234</a></td>
                        <td>ฉุกเฉินแจ้งทุกหน่วย</td>
                        <td>เครื่องใช้ที่1 ไม่ สามารถติดต่ออกันได้</td>
                        <td><span class="label label-warning">กำลังดำเนินงาน</span></td>
                    </tr>

                    </tbody>
                </table>
            </div><!-- /.table-responsive -->
        </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            <a href="javascript::;" class="btn btn-sm btn-info btn-flat pull-left">Create New Ticket</a>
            <a href="javascript::;" class="btn btn-sm btn-default btn-flat pull-right">View All Ticket</a>
        </div><!-- /.box-footer -->
    </div><!-- /.box -->
</div>

