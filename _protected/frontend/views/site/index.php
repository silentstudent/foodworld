<?php

/* @var $this yii\web\View */
$this->title = Yii::t('app', Yii::$app->name);
?>
<div class="site-index">

    <div class="jumbotron">

<center>

        <img src="<?= Yii::getAlias('@web/images/logo.png') ?>" class="img-responsive" style="width:30%" alt="CP food world"></center>

        <p class="lead"><strong>Technical Support </strong>Ticket Tracking Systems</p>

        <p><a class="btn btn-lg btn-success" href="#">Create Ticket</a></p>
    </div>



        <div class="row text-center">

            <div class="col-sm-12">
            <div class="thumbnail">
                <div class="form-group">
                <label class="sr-only" for="exampleInputAmount">ตรวจสอบสถานะงาน</label>
                <div class="input-group">
                    <div class="input-group-addon">ตรวจสอบสถานะงาน</div>
                    <input type="text" class="form-control" id="exampleInputAmount" placeholder="Ticket Number">

                </div>
        <div class="row bs-wizard">

            <div class="col-sm-4 bs-wizard-step complete">
                <div class="text-center bs-wizard-stepnum"><strong>รับงาน</strong></div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center">ช่างผู้รับงาน ชื่อ คุณ ส่องแสง โทร: 092-0099-999</div>
            </div>

            <div class="col-sm-4 bs-wizard-step Active"><!-- complete -->
                <div class="text-center bs-wizard-stepnum"><strong>กำลังดำเนินการ</strong></div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center">สาเหตุ : ..... .....</div>
            </div>

            <div class="col-sm-4 bs-wizard-step disabled"><!-- complete -->
                <div class="text-center bs-wizard-stepnum">เสร็จสิ้น</div>
                <div class="progress"><div class="progress-bar"></div></div>
                <a href="#" class="bs-wizard-dot"></a>
                <div class="bs-wizard-info text-center">แก้ไขโดย : ..........</div>
            </div>

</div>
</div>
            </div>
</div>

<!--            <div class="col-sm-4">-->
<!--                <div class="thumbnail">-->
<!---->
<!---->
<!--                    <img src="image/engineer_close.png" class="img-circle"-->
<!--                         alt="User Image"/>-->
<!--                    <p><strong>แจ้งงานซ่อม</strong></p>-->
<!--                    <button class="btn">Create Tickets</button>-->
<!--                </div>-->
<!--</div>-->

