<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

    $this->title = Yii::t('app', 'engineer');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">

        <section class="content">
           <center> <h1>Engineer Tracking Ticket System Support</h1></center>
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="pull-right">
                            <div class="btn-group">
                                <button type="button" class="btn btn-success btn-filter" data-target="pagado">ใหม่</button>
                                <button type="button" class="btn btn-warning btn-filter" data-target="pendiente">กำลังดำเนินการ</button>
                                <button type="button" class="btn btn-danger btn-filter" data-target="cancelado">ยกเลิก</button>
                                <button type="button" class="btn btn-default btn-filter" data-target="all">ทั้งหมด</button>
                            </div>
                        </div>
                        <div class="table-container">
                            <table class="table table-filter">
                                <tbody>
                                <tr data-status="pagado">
                                    <td>
                                        <div class="ckbox">
                                            <input type="checkbox" id="checkbox1">
                                            <label for="checkbox1"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="star">
                                            <i class="glyphicon glyphicon-star"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right">Febrero 13, 2016</span>
                                                <h4 class="title">
                                                    แจ้งงานซ่อม
                                                    <span class="pull-right pagado">(ใหม่)</span>
                                                </h4>
                                                <p class="summary">แจ้งงานซ่อม</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr data-status="pendiente">
                                    <td>
                                        <div class="ckbox">
                                            <input type="checkbox" id="checkbox3">
                                            <label for="checkbox3"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="star">
                                            <i class="glyphicon glyphicon-star"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right">Febrero 13, 2016</span>
                                                <h4 class="title">
                                                    แจ้งงานซ่อม
                                                    <span class="pull-right pendiente">(กำลังดำเนินการ)</span>
                                                </h4>
                                                <p class="summary">แจ้งงานซ่อม</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr data-status="cancelado">
                                    <td>
                                        <div class="ckbox">
                                            <input type="checkbox" id="checkbox2">
                                            <label for="checkbox2"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="star">
                                            <i class="glyphicon glyphicon-star"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right">Febrero 13, 2016</span>
                                                <h4 class="title">
                                                    แจ้งงานซ่อม
                                                    <span class="pull-right cancelado">(ยกเลิก)</span>
                                                </h4>
                                                <p class="summary">แจ้งงานซ่อม</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr data-status="pagado" class="selected">
                                    <td>
                                        <div class="ckbox">
                                            <input type="checkbox" id="checkbox4" checked>
                                            <label for="checkbox4"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="star star-checked">
                                            <i class="glyphicon glyphicon-star"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right">Febrero 13, 2016</span>
                                                <h4 class="title">
                                                    แจ้งงานซ่อม
                                                    <span class="pull-right pagado">(ใหม่)</span>
                                                </h4>
                                                <p class="summary">แจ้งงานซ่อม</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr data-status="pendiente">
                                    <td>
                                        <div class="ckbox">
                                            <input type="checkbox" id="checkbox5">
                                            <label for="checkbox5"></label>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="javascript:;" class="star">
                                            <i class="glyphicon glyphicon-star"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <div class="media">
                                            <a href="#" class="pull-left">
                                                <img src="https://s3.amazonaws.com/uifaces/faces/twitter/fffabs/128.jpg" class="media-photo">
                                            </a>
                                            <div class="media-body">
                                                <span class="media-meta pull-right">Febrero 13, 2016</span>
                                                <h4 class="title">
                                                    แจ้งงานซ่อม
                                                    <span class="pull-right pendiente">(กำลังดำเนินการ)</span>
                                                </h4>
                                                <p class="summary">แจ้งงานซ่อม</p>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </section>

    </div>
</div>