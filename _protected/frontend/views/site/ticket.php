<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Ticket');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    Create Ticket <small>Technical Support</small></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="name">
                                    Name</label>
                                <input type="text" class="form-control" id="name" placeholder="กรุณาใส่รหัสสาขา" required="required" />
                            </div>
                            <div class="form-group">
                                <label for="subject">
                                    Subject</label>
                                <select id="subject" name="subject" class="form-control" required="required">
                                    <option value="front_end">กรุณาเลือกประเภท</option>
                                    <option value="back_end">ระบบหน้าร้าน</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="subject">
                                    Priority</label>
                                <select id="subject" name="subject" class="form-control" required="required">
                                    <option value="front_end">ระดับความสำคัญ</option>
                                    <option value="back_end">สูงสุด</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                    <input type="email" class="form-control" id="email" placeholder="กรุณาใส่ Email" required="required" /></div>
                            </div>
                            <div class="form-group">
                                <label for="tel">
                                    Tel</label>
                                <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span>
                                </span>
                                    <input type="tel" class="form-control" id="tel" placeholder="กรุณาใส่ เบอร์โทร" required="required" /></div>
                            </div>

                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="Upload">
                                    Picture</label>
                                <div class="input-group image-preview">
                                     <span class="input-group-addon"><span class="glyphicon glyphicon-picture"></span>
                                </span>
                                    <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>
                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="input-file-preview"/> <!-- rename it -->
                    </div>
                </span>
                                </div><!-- /input-group image-preview [TO HERE]-->
                            </div>
                            <div class="form-group">
                                <label for="name">
                                    Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                      placeholder="รายละเอียด "></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                Send Message</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <form>
                <legend><span class="glyphicon glyphicon-globe"></span> Our office</legend>
                <address>
                    <strong><h3>Cp Food World.</h3></strong><br>18th Floor, 'Fortune Town'<br>
                    Phone: 0-2641-1333<br>
                    E-mail: <a href="mailto:#">contact@cpfoodworld.com</a>
                </address>
            </form>
        </div>
    </div>

</div>
