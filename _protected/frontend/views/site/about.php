<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'About');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">

        <section class="content">
            <center> <h1>Engineer Tracking Ticket System Support</h1></center>
            <div class="col-md-12 col-md-offset-0">
                <div class="panel panel-default">
                    <div class="panel-body">
                       <center>
                           <h2> หมายเลข Ticket !2</h2>
                       </center>
                        <form>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="name">
                                            รหัสสาขา</label>
                                        <input type="text" class="form-control" id="name" value="10230" required="required" />
                                    </div>
                                    <div class="form-group">
                                        <label for="subject">
                                            ประเภท</label>
                                        <input type="text" class="form-control" id="typ" value="Internet" required="required" />
                                    </div>
                                    <div class="form-group">
                                        <label for="subject">
                                            ระดับความสำคัญ</label>
                                        <select id="subject" name="subject" class="form-control" required="required">
                                            <option value="back_end">สูงสุด</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="email">
                                            Email</label>
                                        <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                            <input type="email" class="form-control" id="email" value="cpfoodworld@cpf.co.th" required="required" /></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tel">
                                            Tel</label>
                                        <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span>
                                </span>
                                            <input type="tel" class="form-control" id="tel" value="088-234-5678" required="required" /></div>
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="Upload">
                                            Picture</label>
                                        <img src="<?= Yii::getAlias('@web/images/engineer_close.png') ?>" class="img-thumbnail" style="width:30%" alt="CP food world"></center><!-- /input-group image-preview [TO HERE]-->
                                    </div>
                                    <div class="form-group">
                                        <label for="name">
                                            Message</label>
                            <textarea name="message" id="message" class="form-control" rows="9" cols="25" required="required"
                                      placeholder="รายละเอียด "></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                                        Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </section>

    </div>
</div>